import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { ProductService } from "./services/product.service";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ProductTypeService } from "./services/product.type.service";
import { ProductComponent } from "./product/product.component";
import { MatCardModule, MatDialogModule } from "@angular/material";
import { CreateProductComponent } from "./create-product/create-product.component";
import { NgSelectModule } from "@ng-select/ng-select";

const routes: Routes = [
  { path: "product", component: ProductComponent },
  { path: "create-product", component: CreateProductComponent },
  { path: "**", redirectTo: "product" }
];

@NgModule({
  declarations: [AppComponent, ProductComponent, CreateProductComponent],
  imports: [
    BrowserModule,
    MatCardModule,
    ReactiveFormsModule,
    NgSelectModule,
    MatDialogModule,
    BrowserAnimationsModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ProductService, ProductTypeService],
  bootstrap: [AppComponent]
})
export class AppModule {}
