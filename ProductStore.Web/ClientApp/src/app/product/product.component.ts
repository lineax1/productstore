import { Component, OnInit } from "@angular/core";
import { ProductService } from "../services/product.service";
import { GetAllProductView } from "../model/product/get.products.view";
import { GetAllProductTypeView } from "../model/product-type/get.productTypes.view";
import { ProductTypeService } from "../services/product.type.service";
import { MatDialog } from "@angular/material/dialog";
import { CreateProductComponent } from "../create-product/create-product.component";

@Component({
  selector: "app-product",
  templateUrl: "./product.component.html",
  styleUrls: ["./product.component.css"]
})
export class ProductComponent implements OnInit {
  public productList: GetAllProductView;
  public productTypeList: GetAllProductTypeView;

  constructor(
    private productService: ProductService,
    private dialog: MatDialog,
    private productTypeService: ProductTypeService
  ) {
    this.productList = new GetAllProductView();
    this.productTypeList = new GetAllProductTypeView();
  }

  ngOnInit() {
    this.getAll();
    this.getType();
  }
  
  public getAll() {
    this.productService.getAll().subscribe(res => {
      this.productList = res;
    });
  }

  public getType() {
    this.productTypeService.getAll().subscribe(res => {
      this.productTypeList = res;
    });
  }

  public getTypeList(product: { productTypeList: { nameType: string }[] }) {
    let typeString = "";
    for (let i = 0; i < product.productTypeList.length; i++) {
      typeString += product.productTypeList[i].nameType + "  ";
    }
    return typeString;
  }

  public openCreateProduct() {
    const dialogRef = this.dialog.open(CreateProductComponent);
    dialogRef.afterClosed().subscribe(result => {
      this.getAll();
    });
  }
}
