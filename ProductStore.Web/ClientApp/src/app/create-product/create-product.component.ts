import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup } from "@angular/forms";
import { CreateProductView } from "../model/product/create..product.view";
import { ProductService } from "../services/product.service";
import { ProductTypeService } from "../services/product.type.service";
import { GetAllProductTypeView } from "../model/product-type/get.productTypes.view";
import { MatDialogRef } from "@angular/material";

@Component({
  selector: "app-create-product",
  templateUrl: "./create-product.component.html",
  styleUrls: ["./create-product.component.css"]
})
export class CreateProductComponent implements OnInit {
  public formGroup: FormGroup;
  public productTypeList: GetAllProductTypeView;

  constructor(
    private formBuilder: FormBuilder,
    private productTypeService: ProductTypeService,
    private productService: ProductService,
    public dialogRef: MatDialogRef<CreateProductComponent>
  ) {
    this.productTypeList = new GetAllProductTypeView();
  }

  ngOnInit() {
    this.getAllType();
    this.formGroup = this.formBuilder.group({
      name: [""],
      productTypeIdList: [""]
    });
  }

  public onSubmit() {
    if (this.formGroup.invalid) {
      return console.error("Invalid Data");
    }
    const product: CreateProductView = new CreateProductView();
    product.name = this.formGroup.controls.name.value;
    product.productTypeIdList = this.formGroup.controls.productTypeIdList.value;
    this.productService.create(product).subscribe(res => {
      this.dialogRef.close();
    });
  }

  public getAllType() {
    this.productTypeService.getAll().subscribe(res => {
      this.productTypeList = res;
    });
  }

  get f() {
    return this.formGroup.controls;
  }
}
