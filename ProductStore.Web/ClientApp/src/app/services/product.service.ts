import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { GetAllProductView } from "../model/product/get.products.view";
import { environment } from "src/environments/environment";
import { CreateProductView } from "../model/product/create..product.view";
import { Injectable } from "@angular/core";

@Injectable()
export class ProductService {
  constructor(private readonly http: HttpClient) {}

  public getAll(): Observable<GetAllProductView> {
    return this.http.get<GetAllProductView>(
      environment.apiUrl + "/api/product/getall"
    );
  }

  public create(item: CreateProductView): Observable<CreateProductView> {
    return this.http.post<CreateProductView>(
      environment.apiUrl + "/api/product/create",
      item
    );
  }
}
