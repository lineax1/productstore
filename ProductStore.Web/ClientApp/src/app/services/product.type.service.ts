import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { environment } from "src/environments/environment";
import { GetAllProductTypeView } from "../model/product-type/get.productTypes.view";
import { CreateProductTypeView } from "../model/product-type/create.productType.view";
import { Injectable } from "@angular/core";

@Injectable()
export class ProductTypeService {
  constructor(private readonly http: HttpClient) {}

  public getAll(): Observable<GetAllProductTypeView> {
    return this.http.get<GetAllProductTypeView>(
      environment.apiUrl + "/api/producttype/getall"
    );
  }
  
  public create(item: CreateProductTypeView): Observable<CreateProductTypeView> {
    return this.http.post<CreateProductTypeView>(
      environment.apiUrl + "/api/producttype/create",
      item
    );
  }
}
