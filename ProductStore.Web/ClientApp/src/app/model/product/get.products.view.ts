import {  PorductTypeGetAllProductTypeView } from "../product-type/get.productTypes.view";

export class GetAllProductView{
    
    public productList: Array<ProductGetAllProductItemView>;
    constructor()
    {
        this.productList = new Array<ProductGetAllProductItemView>();
    }
}
export class ProductGetAllProductItemView
{
    public id: number;
    public name: string;
    public productTypeList?: PorductTypeGetAllProductTypeView;
}
