﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ProductStore.BusinessLogicLayer.Services.Interfaces;
using ProductStore.BusinessLogicLayer.Views.ProductTypeView;
using System.Threading.Tasks;

namespace ProductStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductTypeController : Controller
    {
        private readonly IProductTypeService _productTypeService;

        public ProductTypeController(IProductTypeService productTypeService)
        {
            _productTypeService = productTypeService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            GetAllProductTypeView getAll = await _productTypeService.GetAll();
            return Ok(getAll);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Create(CreateProductTypeView createProduct)
        {
            await _productTypeService.Create(createProduct);
            return Ok();
        }
    }
}