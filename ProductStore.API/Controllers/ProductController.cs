﻿using Microsoft.AspNetCore.Mvc;
using ProductStore.BusinessLogicLayer.Services.Interfaces;
using ProductStore.BusinessLogicLayer.Views.ProductView;
using System.Threading.Tasks;

namespace ProductStore.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            GetAllProductView getAll = await _productService.GetAll();
            return Ok(getAll);
        }

        [HttpPost]
        public async Task<IActionResult> Create(CreateProductView createProduct)
        {
            await _productService.Create(createProduct);
            return Ok();
        }
    }
}