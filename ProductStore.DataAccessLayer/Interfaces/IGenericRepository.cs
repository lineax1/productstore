﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProductStore.DataAccessLayer.Interfaces
{
    public interface IGenericRepository<TEntity> where TEntity: class
    {
        Task<IEnumerable<TEntity>> GetAll();
        Task<int> Create(TEntity entity);
        Task CreateRange(IEnumerable<TEntity> entity);
    }
}
