﻿using ProductStore.DataAccessLayer.Entities;

namespace ProductStore.DataAccessLayer.Interfaces
{
   public interface IProductTypeProductRepository:IGenericRepository<ProductTypeProduct>
    {

    }
}
