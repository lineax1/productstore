﻿using ProductStore.DataAccessLayer.Entities;

namespace ProductStore.DataAccessLayer.Interfaces
{
    public interface IProductTypeRepository: IGenericRepository<ProductType>
    {

    }
}
