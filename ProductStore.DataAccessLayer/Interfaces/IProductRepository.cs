﻿using ProductStore.DataAccessLayer.Entities;

namespace ProductStore.DataAccessLayer.Interfaces
{
    public interface IProductRepository: IGenericRepository<Product>
    {

    }
}
