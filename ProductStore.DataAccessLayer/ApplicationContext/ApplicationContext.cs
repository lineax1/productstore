﻿using Microsoft.EntityFrameworkCore;
using ProductStore.DataAccessLayer.Entities;

namespace ProductStore.DataAccessLayer
{
    public class ApplicationContext: DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<ProductTypeProduct> ProductTypeProducts { get; set; }

        public ApplicationContext()
        {
            Database.EnsureCreated();
        }
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Product>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<Product>()
                .Property(x => x.Id).ValueGeneratedOnAdd();

            modelBuilder.Entity<ProductType>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<ProductType>()
                .Property(x => x.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<ProductTypeProduct>()
                .HasKey(x => x.Id);
            modelBuilder.Entity<ProductTypeProduct>()
                .Property(x => x.Id);

            modelBuilder.Entity<ProductTypeProduct>()
                .HasOne(x => x.Product)
                .WithMany(x => x.ProductTypeProduct);

            modelBuilder.Entity<ProductTypeProduct>()
                .HasOne(x => x.ProductType)
                .WithMany(x => x.ProductTypeProduct);
        }
    }
}
