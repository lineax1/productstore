﻿using Microsoft.Extensions.Options;
using ProductStore.DataAccessLayer.Config;
using ProductStore.DataAccessLayer.Entities;
using ProductStore.DataAccessLayer.Interfaces;
using ProductStore.DataAccessLayer.Repository.GenericRepository;

namespace ProductStore.DataAccessLayer.Repository.Dapper
{
    public class ProductTypeProductRepository: GenericRepository<ProductTypeProduct>, IProductTypeProductRepository
    {
        public ProductTypeProductRepository(IOptions<ConnectionStrings> connectionString) : base(connectionString, "ProductTypeProducts")
        { }

    }
}
