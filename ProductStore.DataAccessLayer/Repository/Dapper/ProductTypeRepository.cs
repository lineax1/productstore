﻿using Microsoft.Extensions.Options;
using ProductStore.DataAccessLayer.Config;
using ProductStore.DataAccessLayer.Entities;
using ProductStore.DataAccessLayer.Interfaces;
using ProductStore.DataAccessLayer.Repository.GenericRepository;

namespace ProductStore.DataAccessLayer.Repository.Dapper
{
    public class ProductTypeRepository : GenericRepository<ProductType>, IProductTypeRepository
    {
        public ProductTypeRepository(IOptions<ConnectionStrings> connectionString) : base(connectionString, "ProductTypes")
        { }

    }
}
