﻿using Dapper.Contrib.Extensions;
using Microsoft.Extensions.Options;
using ProductStore.DataAccessLayer.Config;
using ProductStore.DataAccessLayer.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace ProductStore.DataAccessLayer.Repository.GenericRepository
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly string _tableName;
        protected readonly string _connectionString;
        protected IDbConnection Connection
        {
            get { return new SqlConnection(_connectionString); }
        }

        public GenericRepository(IOptions<ConnectionStrings> connectionConfig, string tableName)
        {
            var connection = connectionConfig.Value;
            _connectionString = connection.DefaultConnection;
            _tableName = tableName;
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            return await Connection.GetAllAsync<TEntity>();
        }

        public async Task<int> Create(TEntity entity)
        {
            try
            {
                return await Connection.InsertAsync(entity);
            }
            catch (Exception ex) { throw ex; }
        }
        public async Task CreateRange(IEnumerable<TEntity> entity)
        {
            await Connection.InsertAsync(entity);
        }
    }
}
