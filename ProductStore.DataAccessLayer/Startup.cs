﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductStore.DataAccessLayer.Interfaces;
using ProductStore.DataAccessLayer.Repository.Dapper;
using System.Data;
using System.Data.SqlClient;

namespace ProductStore.DataAccessLayer
{
   public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public static void ConfigureServices(IServiceCollection services, string ConnectionString)
        {
            services.AddDbContext<ApplicationContext>(options => options.UseSqlServer(ConnectionString));
            services.AddTransient<IDbConnection>(db => new SqlConnection(ConnectionString));

            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IProductTypeRepository, ProductTypeRepository>();
            services.AddScoped<IProductTypeProductRepository, ProductTypeProductRepository>();

            var serviceProvider = services.BuildServiceProvider();
    
        }
    }
}

