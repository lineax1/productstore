﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ProductStore.DataAccessLayer.Entities
{
    public class BaseEntity
    {
        [Key]
        public int Id { get; set; }
        public  DateTime CreationDate { get; set; }

        public BaseEntity()
        {
            CreationDate = DateTime.UtcNow;
        }
    }
}
