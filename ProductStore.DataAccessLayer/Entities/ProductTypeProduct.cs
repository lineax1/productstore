﻿using Dapper.Contrib.Extensions;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductStore.DataAccessLayer.Entities
{
    public class ProductTypeProduct: BaseEntity
    {
        public int ProductId { get; set; }
        public int ProductTypeId { get; set; }

        [NotMapped]
        [Write(false)]
        public Product Product { get; set; }
        [NotMapped]
        [Write(false)]
        public ProductType ProductType { get; set; }
    }
}
