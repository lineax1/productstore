﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductStore.DataAccessLayer.Entities
{
    public class Product:BaseEntity
    {
        public string Name { get; set; }

        [NotMapped]
        [Write(false)]
        public IEnumerable<ProductTypeProduct> ProductTypeProduct { get; set; }
    }
}
