﻿using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace ProductStore.DataAccessLayer.Entities
{
   public class ProductType:BaseEntity
    {
        public string TypeName { get; set; }

        [NotMapped]
        [Write(false)]
        public IEnumerable<ProductTypeProduct> ProductTypeProduct { get; set; }
    }
}
