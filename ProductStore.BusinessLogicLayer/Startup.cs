﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProductStore.BusinessLogicLayer.Services;
using ProductStore.BusinessLogicLayer.Services.Interfaces;

namespace ProductStore.BusinessLogicLayer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public static void ConfigureServices(IServiceCollection services ,string ConnectionString, IConfiguration Configuration)
        {
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IProductTypeService, ProductTypeService>();

            DataAccessLayer.Startup.ConfigureServices(services, ConnectionString);
        }
    }
}
