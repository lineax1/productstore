﻿using System.Collections.Generic;

namespace ProductStore.BusinessLogicLayer.Views.ProductView
{
    public class GetAllProductView
    {
        public List<ProductGetAllProductViewItem> ProductList { get; set; }
        public GetAllProductView()
        {
            ProductList = new List<ProductGetAllProductViewItem>();
        }

    }
    public class ProductGetAllProductViewItem
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<GetAllProductTypeView> ProductTypeList { get; set; }
    }
    public class GetAllProductTypeView
    {
        public int Id { get; set; }
        public string NameType { get; set; }
    }
}
