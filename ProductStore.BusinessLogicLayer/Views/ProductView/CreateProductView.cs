﻿using System.Collections.Generic;

namespace ProductStore.BusinessLogicLayer.Views.ProductView
{
    public class CreateProductView
    {
        public string Name { get; set; }

        public IEnumerable<int> ProductTypeIdList = new List<int>();
    }
}
