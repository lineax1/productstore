﻿namespace ProductStore.BusinessLogicLayer.Views.ProductTypeView
{
    public class CreateProductTypeView
    {
        public string TypeName { get; set; }
    }
}
