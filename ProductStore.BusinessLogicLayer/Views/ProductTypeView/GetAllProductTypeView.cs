﻿using System.Collections.Generic;

namespace ProductStore.BusinessLogicLayer.Views.ProductTypeView
{
    public class GetAllProductTypeView
    {
        public List<ProductTypeGetAllProductTypeViewItem> ProductTypeList { get; set; }
        public GetAllProductTypeView()
        {
            ProductTypeList = new List<ProductTypeGetAllProductTypeViewItem>();
        }

    }

    public class ProductTypeGetAllProductTypeViewItem
    {
        public int Id { get; set; }
        public string TypeName { get; set; }

        public IEnumerable<GetAllProductView> ProductList { get; set; }
    }

    public class GetAllProductView
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
