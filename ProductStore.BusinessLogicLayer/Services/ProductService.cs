﻿using ProductStore.BusinessLogicLayer.Services.Interfaces;
using ProductStore.BusinessLogicLayer.Views.ProductView;
using ProductStore.DataAccessLayer.Entities;
using ProductStore.DataAccessLayer.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.BusinessLogicLayer.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;
        private readonly IProductTypeRepository _productTypeRepository;
        private readonly IProductTypeProductRepository _productTypeProductRepository;
        public ProductService(IProductRepository productRepository, IProductTypeProductRepository productTypeProductRepository, IProductTypeRepository productTypeRepository)
        {
            _productRepository = productRepository;
            _productTypeProductRepository = productTypeProductRepository;
            _productTypeRepository = productTypeRepository;
        }

        public async Task<GetAllProductView> GetAll()
        {
            IEnumerable<Product> products = await _productRepository.GetAll();
            IEnumerable<ProductType> productType = await _productTypeRepository.GetAll();
            IEnumerable<ProductTypeProduct> productTypeProduct = await _productTypeProductRepository.GetAll();


            List<ProductGetAllProductViewItem> productView = products.Select(product => new ProductGetAllProductViewItem
            {
                Name = product.Name,
                Id = product.Id,
                ProductTypeList = MapRange(productType.Where(type => productTypeProduct.Where(x => x.ProductId == product.Id).Select(c => c.ProductTypeId).Contains(type.Id)))
            }).ToList();
            var result = new GetAllProductView() { ProductList = productView };
            result.ProductList = productView;
            return result;
        }

        public async Task Create(CreateProductView createProductView)
        {
            var product = new Product
            {
                Name = createProductView.Name
            };

            int productId = await _productRepository.Create(product);

            List<ProductTypeProduct> productTypeList = (createProductView.ProductTypeIdList.Select(x => new ProductTypeProduct()
            {
                ProductId = productId,
                ProductTypeId = x
            })).ToList();
            await _productTypeProductRepository.CreateRange(productTypeList);
        }

        private List<GetAllProductTypeView> MapRange(IEnumerable<ProductType> productTypes)
        {
            return productTypes.Select(productType => new GetAllProductTypeView()
            {
                Id = productType.Id,
                NameType = productType.TypeName
            }).ToList();
        }
    }
}
