﻿using ProductStore.BusinessLogicLayer.Views.ProductTypeView;
using System.Threading.Tasks;

namespace ProductStore.BusinessLogicLayer.Services.Interfaces
{
    public interface IProductTypeService
    {
        Task<GetAllProductTypeView> GetAll();
        Task Create(CreateProductTypeView createProductType);
    }
}
