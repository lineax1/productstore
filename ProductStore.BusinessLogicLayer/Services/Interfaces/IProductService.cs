﻿using ProductStore.BusinessLogicLayer.Views.ProductView;
using System.Threading.Tasks;

namespace ProductStore.BusinessLogicLayer.Services.Interfaces
{
    public interface IProductService
    {
        Task<GetAllProductView> GetAll();
        Task Create(CreateProductView createProduct);
    }
}
