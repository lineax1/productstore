﻿using ProductStore.BusinessLogicLayer.Services.Interfaces;
using ProductStore.BusinessLogicLayer.Views.ProductTypeView;
using ProductStore.DataAccessLayer.Entities;
using ProductStore.DataAccessLayer.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductStore.BusinessLogicLayer.Services
{
    public class ProductTypeService : IProductTypeService
    {
        private readonly IProductTypeRepository _productTypeRepository;
        public ProductTypeService(IProductTypeRepository productTypeRepository)
        {
            _productTypeRepository = productTypeRepository;
        }

        public async Task<GetAllProductTypeView> GetAll()
        {
            var productTypes = await _productTypeRepository.GetAll();
            List<ProductTypeGetAllProductTypeViewItem> productTypeList = productTypes.Select(x => new ProductTypeGetAllProductTypeViewItem
            {
                TypeName = x.TypeName,
                Id = x.Id
            }).ToList();
            var result = new GetAllProductTypeView() { ProductTypeList = productTypeList };
            result.ProductTypeList = productTypeList;
            return result;
        }
        public async Task Create(CreateProductTypeView createProductTypeView)
        {
            await _productTypeRepository.Create(new ProductType() { TypeName = createProductTypeView.TypeName });
        }
    }
}
